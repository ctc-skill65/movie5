<?php

function conf($key) {
    global $config;
    return isset($config[$key]) ? $config[$key] : null;
}

function url($path = '') {
    return conf('site_url') . $path;
}

function redirect($path = '') {
    header('Location: ' . url($path));
    exit;
}

function get($key) {
    return isset($_GET[$key]) ? $_GET[$key] : null;
}

function post($key) {
    return isset($_POST[$key]) ? $_POST[$key] : null;
}

function setFlash($key, $value) {
    $_SESSION['flash'][$key] = $value;
}

function getFlash($key, $unset = false) {
    global $flash;

    if (isset($_SESSION['flash'][$key])) {
        $value = $_SESSION['flash'][$key];
        if ($unset) {
            unset($_SESSION['flash'][$key]);
        }
        return $value;
    } elseif (isset($flash[$key])) {
        return $flash[$key];
    }

    return null;
}

function setAlert($status, $message) {
    setFlash('alert', [
        'status' => $status,
        'message' => $message
    ]);
}

function showAlert() {
    $alert = getFlash('alert', true);

    if (empty($alert)) {
        return;
    }

    $status = $alert['status'];
    $message = $alert['message'];
    ?>
    <script>
        window.onload = () => {
            alert("<?= $message ?>");
        }
    </script>
    <?php
}


function upload($key, $dir = '/storage') {
    if (empty($_FILES[$key]['name']) || empty($_FILES[$key]['tmp_name'])) {
        return false;
    }

    $path = $dir . '/';
    $path .= uniqid();
    $path .= '.' . pathinfo($_FILES[$key]['name'], PATHINFO_EXTENSION);
    
    $to_path = ROOT . $path;

    $up = move_uploaded_file($_FILES[$key]['tmp_name'], $to_path);
    if (!is_file($to_path)) {
        return false;
    }

    return $path;
}

function clickConfirm($message) {
    return "onclick=\"return confirm('{$message}')\"";
}


function db_result($sql) {
    global $db;

    $qr = $db->query($sql);
    $result = [];
    while ($row = $qr->fetch_assoc()) {
        $result[] = $row;
    }
    return $result;
}

function db_row($sql) {
    global $db;

    $qr = $db->query($sql);
    return $qr->fetch_assoc();
}

function checkLogin() {
    global $db, $user, $user_id;

    $login_path = '/auth/login.php';
    if (empty($_SESSION['user_id']))
        redirect($login_path);
    
    $user_id = $_SESSION['user_id'];
    $user = db_row("SELECT * FROM `users` WHERE `user_id`='$user_id'");

    if (empty($user) || $user['status'] !== '1') {
        redirect($login_path);
    }
}

function checkAuth($user_type) {
    global $user;
    checkLogin();

    if (empty($user) || $user['user_type'] !== $user_type) {
        redirect('/auth/login.php');
    }
}
