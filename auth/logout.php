<?php
require_once __DIR__ . '/../boot.php';

session_destroy();
session_start();

setAlert('success', 'ออกจากระบบสำเร็จ');
redirect('/auth/login.php');
