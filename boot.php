<?php

define('ROOT', __DIR__);
define('INC', ROOT . '/include');


$config = require ROOT . '/config.php';

date_default_timezone_set("Asia/Bangkok");

session_start();
$flash;
if (isset($_SESSION['flash'])) {
    $flash = $_SESSION['flash'];
    unset($_SESSION['flash']);
}

require_once INC . '/common.php';
require_once INC . '/database.php';
