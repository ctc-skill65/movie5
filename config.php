<?php

return [
    'site_url' => 'http://skill65.local/movie5',

    'db_host' => 'localhost',
    'db_user' => 'root',
    'db_password' => 'root',
    'db_name' => 'skill65_movie5',
    'db_charset' => 'utf8mb4'
];
