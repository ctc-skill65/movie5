<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/users/list.php';

$action = get('action');
$id = get('id');

switch ($action) {
    case 'delete':
        $sql = "DELETE FROM `theater_seats` WHERE `theater_seat_id`='{$id}'";
        break;
}

if (!empty($sql)) {
    $db->query($sql);
}

$items = db_result("SELECT * FROM `theater_seats`");
ob_start();
?>

<?= showAlert() ?>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>ชื่อที่นั่งโรงภาพยนตร์</th> 
            <th>จัดการที่นั่ง</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['theater_seat_id'] ?></td>
                <td><?= $item['seat_name'] ?></td>
                <td> 
                    <a href="?action=delete&id=<?= $item['theater_seat_id'] ?>" <?= clickConfirm("คุณต้องการลบที่นั่งโรงภาพยนตร์ {$item['seat_name']} หรือไม่") ?>>ลบ</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>


<?php
$layout_page = ob_get_clean();
$page_name = 'รายการที่นั่งโรงภาพยนตร์';
require ROOT . '/admin/layout.php';
