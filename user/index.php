<?php
require_once __DIR__ . '/../boot.php';
checkAuth('user');

ob_start();
?>
<h2>ยินดีต้อนรับ <?= $user['firstname'] . ' ' . $user['lastname'] ?></h2>
<?php
$layout_page = ob_get_clean();
$page_name = 'หน้าหลัก';
require ROOT . '/user/layout.php';
