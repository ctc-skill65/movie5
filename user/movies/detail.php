<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$id = get('id');

if (!isset($id)) {
    redirect('/user/movies/search.php');
}

$page_path = "/user/movies/detail.php?id={$id}";

if (!empty($_POST)) {
    // if (!empty($_FILES['poster']['name'])) {
    //     $poster = upload('poster', '/storage/posters');
    //     if ($poster) {
    //         $db->query("UPDATE `movies` SET `poster`='{$poster}' WHERE `movie_id`='{$id}'");
    //     }
    // }
    

    // $qr = $db->query("UPDATE `movies` SET `name`='{$_POST['name']}' WHERE `movie_id`='{$id}'");
    // if ($qr) {
    //     setAlert('success', "แก้ไขภาพยนตร์สำเร็จเรียบร้อย");
    // } else {
    //     setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขภาพยนตร์ได้");
    // }
    redirect($page_path);
}

$data = db_row("SELECT * FROM `movies` WHERE `movie_id`='{$id}'");
$movie_times = db_result("SELECT * FROM `movie_times` WHERE `movie_id`='{$id}'");

ob_start();
?>

<?= showAlert() ?>
<img src="<?= url($data['poster']) ?>" alt="" style="
    max-width: 22rem;
">
<p>
    รหัสภาพยนตร์: <?= $data['movie_id'] ?>
    <br>
    ชื่อภาพยนตร์: <?= $data['name'] ?>
</p>
<hr>
<form method="post">
    <table>
        <thead>
            <tr>
                <th>รหัสเวลาฉาย</th>
                <th>วันเวลาเริ่มฉาย</th>
                <th>เลือกเวลาฉาย</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($movie_times as $item) : ?>
                <tr>
                    <td><?= $item['movie_time_id'] ?></td>
                    <td><?= $item['start_time'] ?></td>
                    <td>
                        <input type="radio" name="movie_time" id="movie_time" value="<?= $item['movie_time_id'] ?>" required="" style="
                            transform: scale(1.5);
                        ">
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <button type="submit">
        จองที่นั่ง
    </button>
</form>


<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขภาพยนตร์';
require ROOT . '/user/layout.php';
