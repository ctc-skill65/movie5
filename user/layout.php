<?php
ob_start();
?>
<h1><?= isset($page_name) ? $page_name : null ?></h1>
<p>
    <?= $user['firstname'] . ' ' . $user['lastname'] ?> (ผู้ใช้งานระบบ)
</p>

<nav>
    <h2>เมนู</h2>
    <ul>
        <li><a href="<?= url('/user/index.php') ?>">หน้าหลัก</a></li>
        <li>จองที่นั่งโรงภาพยนตร์
            <ul>
                <li><a href="<?= url('/user/movies/search.php') ?>">ค้นหาภาพยนตร์</a></li>
            </ul>
        </li>
        <li>ข้อมูลส่วนตัว
            <ul>
                <li><a href="<?= url('/user/profile/edit.php') ?>">แก้ไขข้อมูลส่วนตัว</a></li>
                <li><a href="<?= url('/user/profile/edit-pass.php') ?>">แก้ไขรหัสผ่าน</a></li>
                <li><a href="<?= url('/auth/logout.php') ?>" <?= clickConfirm('คุณต้องการออกจากระบบหรือไม่') ?>>ออกจากระบบ</a></li>
            </ul>
        </li>
    </ul>
</nav>

<hr>

<main>
    <?= isset($layout_page) ? $layout_page : null ?>
</main>
<?php
$layout_body = ob_get_clean();
if (isset($page_name)) {
    $layout_title = $page_name;
}
require INC . '/base_layout.php';
